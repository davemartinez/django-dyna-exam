from django.conf.urls import url

from . import views

app_name = 'quizzes'
urlpatterns = [
    url(r'^$', views.QuizIndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)$', views.QuizDetailView.as_view(), name='detail'),
    url(
        r'^(?P<pk>[0-9]+)/quiz/(?P<take>[0-9]+)$',
        views.QuestionQuizView.as_view(),
        name='quiz'
    ),
    url(r'^quiz/create/$', views.QuizCreateView.as_view(), name='create'),
    url(
        r'^quiz/usercreated/$',
        views.QuizCreatedListView.as_view(),
        name='usercreated'
    ),
    url(
        r'^quiz/usercreated/(?P<pk>[0-9]+)/update$',
        views.QuizUpdateView.as_view(),
        name='quiz_update'
    ),
    url(
        r'^quiz/usercreated/(?P<pk>[0-9]+)/delete$',
        views.QuizDeleteView.as_view(),
        name='quiz_delete'
    ),
    url(
        r'^quiz/usercreated/(?P<pk>[0-9]+)/$',
        views.QuizCreatedDetailView.as_view(),
        name='usercreated_detail'
    ),
    url(
        r'^quiz/usercreated/(?P<pk>[0-9]+)/createquestion/$',
        views.QuestionCreateView.as_view(),
        name='question_create'
    ),
    url(
        r'^quiz/usercreated/(?P<question_id>[0-9]+)/updatequestion/',
        views.QuestionUpdateView.as_view(),
        name='question_update'
    ),
    url(
        r'^quiz/usercreated/(?P<question_id>[0-9]+)/deletequestion/',
        views.QuestionDeleteView.as_view(),
        name='question_delete'
    ),
    url(
        r'^quiz/usercreated/(?P<question_id>[0-9]+)/createchoice/$',
        views.ChoiceCreateView.as_view(),
        name='choice_create'
    ),
    url(
        r'^quiz/usercreated/(?P<choice_id>[0-9]+)/updatechoice/$',
        views.ChoiceUpdateView.as_view(),
        name='choice_update'
    ),
    url(
        r'^quiz/usercreated/(?P<choice_id>[0-9]+)/deletechoice/$',
        views.ChoiceDeleteView.as_view(),
        name='choice_delete'
    )
]
