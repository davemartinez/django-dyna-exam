import datetime

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.utils.datastructures import MultiValueDictKeyError
from django.views.generic import DetailView, ListView, View

from takes.models import Answer, Take

from .forms import ChoiceCreateForm, QuestionCreateForm, QuizCreateForm
from .models import Choice, Question, Quiz


# Create your views here.
class QuizIndexView(ListView):
    model = Quiz
    template_name = 'quizzes/index.html'
    context_object_name = 'quiz_list'
    paginate_by = 20

    def get_queryset(self):
        return Quiz.objects.filter(
            date_created__lte=timezone.now(),
            expiration_date__gte=timezone.now(),
            published__exact=True
        ).order_by('-date_created')


class QuizDetailView(DetailView):
    model = Quiz
    template_name = 'quizzes/detail.html'

    def get_queryset(self):
        return Quiz.objects.filter(expiration_date__gte=timezone.now())

    def post(self, request, pk):
        if not request.user.is_authenticated():
            messages.warning(
                self.request, 'You must be logged in to take a quiz'
            )
            return HttpResponseRedirect(reverse('accounts:login'))
        user = request.user
        quiz = get_object_or_404(Quiz, pk=pk)
        take = Take.objects.create(user=user, quiz=quiz)
        return HttpResponseRedirect(reverse('quizzes:quiz', kwargs={
            'pk': quiz.id,
            'take': take.id
        }))


class QuestionQuizView(LoginRequiredMixin, View):
    def get(self, request, pk, take):
        take = get_object_or_404(Take, pk=take)
        question_list = get_object_or_404(Quiz, pk=pk).question_set.all()
        paginator = Paginator(question_list, 1)
        if (request.user != take.user or question_list.count() == 0 or not
                take.quiz.published):
            messages.add_message(
                request, messages.INFO, 'You are not allowed to view the quiz'
            )
            return HttpResponseRedirect(reverse('accounts:dashboard'))

        page = request.GET.get('question')
        try:
            questions = paginator.page(page)
        except PageNotAnInteger:
            questions = paginator.page(1)
        except EmptyPage:
            questions = paginator.page(paginator.num_pages)
        return render(request, 'quizzes/quiz.html', {'questions': questions})

    def post(self, request, pk, take):
        next = request.POST.get('next', '/')
        if request.GET.get('question'):
            next = next + "?question=" + request.GET.get('question')
        try:
            choice = get_object_or_404(Choice, pk=request.POST['answer'])
            session = get_object_or_404(Take, pk=take)
            question = choice.question
            Answer.objects.create(
                selected_choice=choice,
                question=question,
                take=session
            )
            if request.POST.get('finish'):
                return HttpResponseRedirect(reverse('accounts:dashboard'))
        except MultiValueDictKeyError:
            return HttpResponseRedirect(next)

    def handle_no_permission(self):
        messages.warning(
            self.request, 'You are not allowed to view this page.'
        )
        return super(QuestionQuizView, self).handle_no_permission()


class QuizCreateView(LoginRequiredMixin, View):
    def get(self, request):
        form = QuizCreateForm()
        return render(
            request, 'quizzes/quiz_create.html',
            {'form': form, 'action': 'Create'}
        )

    def post(self, request):
        form = QuizCreateForm(request.POST)
        if form.is_valid():
            quiz = form.save(commit=False)
            expiration_date = datetime.datetime(
                int(request.POST.get('expiration_date_year')),
                int(request.POST.get('expiration_date_month')),
                int(request.POST.get('expiration_date_day')),
            )
            quiz.expiration_date = expiration_date
            quiz.created_by = request.user
            quiz.save()
        return HttpResponseRedirect(reverse('quizzes:index'))


class QuizUpdateView(LoginRequiredMixin, View):
    def get(self, request, pk):
        quiz = get_object_or_404(Quiz, pk=pk)
        if quiz.created_by != request.user:
            messages.add(
                request, messages.INFO, 'You are not allowed to view the quiz'
            )
            return HttpResponseRedirect(reverse('quizzes:usercreated'))
        form = QuizCreateForm(model_to_dict(quiz))
        return render(
            request, 'quizzes/quiz_create.html',
            {'form': form, 'action': 'Update', 'quiz': quiz}
         )

    def post(self, request, pk):
        form = QuizCreateForm(request.POST)
        if form.is_valid():
            quiz = form.save(commit=False)
            expiration_date = datetime.datetime(
                int(request.POST.get('expiration_date_year')),
                int(request.POST.get('expiration_date_month')),
                int(request.POST.get('expiration_date_day')),
            )
            quiz.id = pk
            quiz.expiration_date = expiration_date
            quiz.created_by = request.user
            quiz.save()
        return HttpResponseRedirect(reverse('quizzes:usercreated'))


class QuizDeleteView(LoginRequiredMixin, View):
    def get(self, request, pk):
        quiz = get_object_or_404(Quiz, pk=pk)
        if quiz.created_by != request.user:
            messages.add(
                request, messages.INFO, 'You are not allowed to view the quiz'
            )
            return HttpResponseRedirect(reverse('quizzes:usercreated'))
        quiz.delete()
        return HttpResponseRedirect(reverse('quizzes:usercreated'))


class QuizCreatedListView(LoginRequiredMixin, ListView):
    model = Quiz
    template_name = "quizzes/quiz_created_index.html"

    def get_queryset(self):
        return Quiz.objects.filter(
            created_by__exact=self.request.user
        ).order_by('-date_created')


class QuizCreatedDetailView(LoginRequiredMixin, View):
    def get(self, request, pk):
        quiz = get_object_or_404(Quiz, pk=pk)
        return render(
            request, 'quizzes/quiz_created_detail.html',
            {'quiz': quiz}
        )


class QuestionCreateView(LoginRequiredMixin, View):
    def get(self, request, pk):
        quiz = get_object_or_404(Quiz, pk=pk)
        form = QuestionCreateForm()
        return render(
            request, 'quizzes/question_create.html',
            {'quiz': quiz, 'form': form, 'action': 'Add'}
        )

    def post(self, request, pk):
        form = QuestionCreateForm(request.POST)
        question = form.save(commit=False)
        question.quiz = get_object_or_404(Quiz, pk=pk)
        question.save()
        return HttpResponseRedirect(
            reverse('quizzes:usercreated_detail', kwargs={
                'pk': pk
            })
        )


class QuestionUpdateView(LoginRequiredMixin, View):
    def get(self, request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        if request.user != question.quiz.created_by:
            messages.add(
                request, messages.INFO, 'You are not allowed to view the quiz'
            )
            return HttpResponseRedirect(
                reverse(
                    'quizzes:usercreated_detail',
                    kwargs={'pk': question.quiz.id}
                )
            )
        form = QuestionCreateForm(model_to_dict(question))
        return render(
            request, 'quizzes/question_create.html',
            {'form': form, 'action': 'Update', 'question_id': question.id}
        )

    def post(self, request, question_id):
        form = QuestionCreateForm(request.POST)
        if form.is_valid():
            question_object = get_object_or_404(Question, pk=question_id)
            question = form.save(commit=False)
            question.id = question_object.id
            question.quiz = question_object.quiz
            question.save()

        return HttpResponseRedirect(
            reverse('quizzes:usercreated_detail', kwargs={
                'pk': question.quiz.id,
            })
        )


class QuestionDeleteView(LoginRequiredMixin, View):
    def get(self, request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        if question.quiz.created_by != request.user:
            messages.add(
                request, messages.INFO, 'You are not allowed to view the quiz'
            )
            return HttpResponseRedirect(reverse('quizzes:usercreated'))
        question.delete()
        return HttpResponseRedirect(reverse(
            'quizzes:usercreated_detail', kwargs={'pk': question.quiz.id}
            ))


class ChoiceCreateView(LoginRequiredMixin, View):
    def get(self, request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        form = ChoiceCreateForm()
        return render(
            request, 'quizzes/choice_create.html',
            {'question': question, 'form': form, 'action': 'Add'}
        )

    def post(self, request, question_id):
        form = ChoiceCreateForm(request.POST)
        choice = form.save(commit=False)
        choice.question = get_object_or_404(Question, pk=question_id)
        choice.save()
        return HttpResponseRedirect(
            reverse('quizzes:usercreated_detail', kwargs={
                'pk': choice.question.quiz.id
            })
        )


class ChoiceUpdateView(LoginRequiredMixin, View):
    def get(self, request, choice_id):
        choice = get_object_or_404(Choice, pk=choice_id)
        question = choice.question
        form = ChoiceCreateForm(model_to_dict(choice))
        return render(
            request, 'quizzes/choice_create.html', {
                'question': question, 'form': form,
                'action': 'Update', 'choice_id': choice.id
            }
        )

    def post(self, request, choice_id):
        form = ChoiceCreateForm(request.POST)
        if form.is_valid():
            choice_object = get_object_or_404(Choice, pk=choice_id)
            choice = form.save(commit=False)
            choice.id = choice_object.id
            choice.question = choice_object.question
            choice.save()

        return HttpResponseRedirect(
            reverse('quizzes:usercreated_detail', kwargs={
                'pk': choice.question.quiz.id,
            })
        )


class ChoiceDeleteView(LoginRequiredMixin, View):
    def get(self, request, choice_id):
        choice = get_object_or_404(Choice, pk=choice_id)
        if choice.question.quiz.created_by != request.user:
            messages.add(
                request, messages.INFO, 'You are not allowed to view the quiz'
            )
            return HttpResponseRedirect(reverse(
                'quizzes:usercreated_detail',
                kwargs={'pk': choice.question.quiz.id}
            ))
        choice.delete()
        return HttpResponseRedirect(reverse(
            'quizzes:usercreated_detail',
            kwargs={'pk': choice.question.quiz.id}
        ))
