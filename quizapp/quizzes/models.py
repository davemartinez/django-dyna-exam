import datetime

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


# Create your models here.
def timezone_now_plus_one_day():
    return timezone.now() + datetime.timedelta(days=1)


class Quiz(models.Model):
    title = models.CharField(max_length=200)
    date_created = models.DateTimeField(default=timezone.now)
    expiration_date = models.DateTimeField(default=timezone_now_plus_one_day)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    published = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "quizzes"

    def __str__(self):
        return self.title


class Question(models.Model):
    question_text = models.CharField(max_length=256)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)

    def __str__(self):
        return self.question_text


class Choice(models.Model):
    choice_text = models.CharField(max_length=256)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return self.choice_text
