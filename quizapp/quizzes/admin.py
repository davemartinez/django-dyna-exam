import nested_admin
from django.contrib import admin

from .models import Choice, Question, Quiz


# Register your models here.
class QuestionChoiceInline(nested_admin.NestedTabularInline):
    model = Choice
    extra = 1


class QuizQuestionInline(nested_admin.NestedStackedInline):
    model = Question
    inlines = [QuestionChoiceInline]
    extra = 1


class QuizAdmin(nested_admin.NestedModelAdmin):
    model = Quiz
    inlines = [QuizQuestionInline]


admin.site.register(Quiz, QuizAdmin)
