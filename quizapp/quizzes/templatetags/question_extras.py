from django import template

from takes.utils import is_question_correct

register = template.Library()


@register.filter
def is_correct(value, take):
    return is_question_correct(take, value)


@register.filter
def has_questions(value):
    return value.question_set.count() != 0
