from django import forms
from django.forms import ModelForm

from .models import Choice, Question, Quiz


class QuizCreateForm(ModelForm):
    title = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'Enter quiz title',
        'class': 'form-control'
    }))
    expiration_date = forms.DateTimeField(
        widget=forms.SelectDateWidget(
            empty_label=("Choose Year", "Choose Month", "Choose Day"),
        ),
    )

    class Meta:
        model = Quiz
        fields = ['title', 'expiration_date', 'published']


class QuestionCreateForm(ModelForm):
    question_text = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'Enter question',
        'class': 'form-control'
    }))

    class Meta:
        model = Question
        fields = ['question_text']


class ChoiceCreateForm(ModelForm):
    choice_text = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'Enter choice',
        'class': 'form-control'
    }))
    is_correct = forms.BooleanField(initial=False, required=False)

    class Meta:
        model = Choice
        fields = ['choice_text', 'is_correct']
