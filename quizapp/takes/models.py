from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from quizzes.models import Choice, Question, Quiz

from .utils import is_question_correct


# Create your models here.
class Take(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    date_taken = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.user.username + " - " + self.quiz.title

    def get_score(self):
        questions = self.quiz.question_set.all()
        score = 0
        for question in questions:
            if is_question_correct(self, question):
                score = score + 1

        return "%d out of %d" % (score, questions.count())

    score = property(get_score)


class Answer(models.Model):
    selected_choice = models.ForeignKey(Choice, on_delete=models.CASCADE)
    take = models.ForeignKey(Take, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    def __str__(self):
        return self.selected_choice.choice_text
