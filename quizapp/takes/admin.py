from django.contrib import admin

from .models import Answer, Take

# Register your models here.


class TakeAdmin(admin.ModelAdmin):
    fields = ['user', 'quiz', 'date_taken']
    list_display = ['user', 'quiz', 'score', 'date_taken']


class AnswerAdmin(admin.ModelAdmin):
    fields = ['selected_choice',  'take', 'question']
    list_display = ['selected_choice',  'take', 'question']


admin.site.register(Take, TakeAdmin)
admin.site.register(Answer, AnswerAdmin)
