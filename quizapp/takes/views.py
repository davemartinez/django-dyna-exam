from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.generic import View

from .models import Take


# Create your views here.
class ResultsView(View):
    def get(self, request, pk):
        take = get_object_or_404(Take, pk=pk)
        if take.user == request.user:
            return render(request, 'takes/results.html', {'take': take})
        return HttpResponseRedirect(reverse('accounts:dashboard'))
