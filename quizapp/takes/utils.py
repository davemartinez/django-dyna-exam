def is_question_correct(take, question):
    """Returns a boolean value if the question is correct on the take
    Takes a Take(Quiz Session) and the question to check
    """

    choices = question.choice_set.filter(is_correct__exact=True)
    answers = question.answer_set.filter(
        take__user__exact=take.user,
        take__exact=take
    )
    set_choices = set([choice.choice_text for choice in choices])
    set_answers = set(
        [answer.selected_choice.choice_text for answer in answers]
    )
    return set_choices == set_answers
