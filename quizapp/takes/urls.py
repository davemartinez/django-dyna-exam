from django.conf.urls import url

from . import views

app_name = 'takes'
urlpatterns = [
    url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results')
]
