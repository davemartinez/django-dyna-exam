from django.conf.urls import url
from django.contrib.auth.views import logout

from . import views

app_name = 'accounts'
urlpatterns = [
    url(r'^login/$', views.AccountLoginView.as_view(), name='login'),
    url(r'^logout/$', logout, {'next_page': '/accounts/login'}, name='logout'),
    url(r'^register/$', views.AccountRegisterView.as_view(), name='register'),
    url(r'^dashboard/$', views.AccountDashboardView.as_view(), name='dashboard')
]
