from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import ListView, View

from takes.models import Take

from .forms import LoginForm, RegistrationForm


# Create your views here.
class AccountLoginView(View):
    def get(self, request):
        form = LoginForm()
        args = {'form': form}
        return render(request, 'accounts/login.html', args)

    def post(self, request):
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse('quizzes:index'))

        messages.add_message(
            request, 50, 'Wrong login credentials.', extra_tags='danger'
        )
        return render(request, 'accounts/login.html', {'form': form})


class AccountRegisterView(View):
    def get(self, request):
        form = RegistrationForm()
        args = {'form': form}
        return render(request, 'accounts/register.html', args)

    def post(self, request):
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Successfully Registered. You can now login.'
             )
            return HttpResponseRedirect(reverse('accounts:login'))
        return render(request, 'accounts/register.html', {'form': form})


class AccountDashboardView(LoginRequiredMixin, ListView):
    model = Take
    paginate_by = 10
    template_name = 'accounts/dashboard.html'
    context_object_name = 'takes'

    def get_queryset(self):
        return Take.objects.filter(
            user__exact=self.request.user
        ).order_by('-date_taken')

    def handle_no_permission(self):
        messages.warning(
            self.request, 'You must login first to view dashboard'
        )
        return super(AccountDashboardView, self).handle_no_permission()
